#pragma once

#include <queue>
#include <utility>
#include <ctime>

namespace orl
{

class SpanData
{
	friend class SpanDataTest;

public:
	void addTick(time_t t, double v);
	void moveSpanWindow(time_t t, bool bCheckEqual=true);
	double getMaxAvg() const {return m_fMaxAvg;}
	time_t getStartTimeStamp() const {return m_tMaxAvg;}
	bool isValid() const {return m_bValid;}

private:
	struct TICK
	{
		time_t _t = 0;
		double _v = 0;
		int _n = 1;

		TICK(time_t t, double v, int n) : _t(t), _v(v), _n(n) {}
		void addValue(double v) {_v+=v; ++_n;}
	};

	static constexpr time_t SPAN_WINDOW = 3600; //1h

	std::queue<TICK> m_qTicks;
	double m_fAvg = 0;
	double m_fMaxAvg = 0;
	time_t m_tMaxAvg = 0;
	time_t m_tLast = 0;
	int m_nTickCount = 0;
	bool m_bValid = false;
};

} /* namespace orl */

