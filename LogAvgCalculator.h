#pragma once

#include "SpanData.h"

#include <unordered_map>

namespace orl
{

class istream;

class LogAvgCalculator
{
	friend class LogAvgCalculatorTest;

public:
	bool maxAverage(const char *filename, time_t& startTime, double& maxAverage, std::string& userName);

private:
	bool parseStream(std::istream& is, time_t& startTime, double& maxAverage, std::string& userName);
	bool parseLine(const std::string strLine, time_t& timestamp, double& value, std::string& user) const;

	typedef std::unordered_map<std::string, SpanData> MAP_USERDATA;

	MAP_USERDATA m_mapUsers;
};

} /* namespace orl */
