#include "SpanData.h"
#include <stdexcept>

namespace orl
{

void SpanData::addTick(time_t t, double v)
{
	moveSpanWindow(t, false);

	//update average
	m_fAvg = (m_fAvg * m_nTickCount + v ) / (m_nTickCount + 1);

	//create new timestamp or update already existed
	if (!m_nTickCount || m_qTicks.back()._t != t)
		m_qTicks.emplace(t, v, 1);
	else
	    m_qTicks.back().addValue(v);

	++m_nTickCount;
}

void SpanData::moveSpanWindow(time_t t, bool bCheckEqual)
{
	if (t < m_tLast)
		throw std::runtime_error("Unordered tick");

	m_tLast = t;

	//remove items older then 1h and update average and maxAverage
	const TICK* pFirstTick = m_nTickCount ? &m_qTicks.front() : nullptr;
	while (pFirstTick && (t - pFirstTick->_t > SPAN_WINDOW))
	{
		if (m_fAvg > m_fMaxAvg)
		{
			m_fMaxAvg = m_fAvg;
			m_tMaxAvg = pFirstTick->_t;
			m_bValid = true;
		}

		if (m_qTicks.size() > 1)
			m_fAvg = (m_fAvg * m_nTickCount - pFirstTick->_v) / (m_nTickCount - pFirstTick->_n);
		else
			m_fAvg = 0;

		m_nTickCount -= pFirstTick->_n;
		m_qTicks.pop();

		pFirstTick = m_nTickCount ? &m_qTicks.front() : nullptr;
	}

	//update maxAvg if span between first tick's time and new time is exact 1h
	//typically at the end of processing
	if (bCheckEqual && pFirstTick && (t - pFirstTick->_t == SPAN_WINDOW) && (m_fAvg > m_fMaxAvg))
	{
		m_fMaxAvg = m_fAvg;
		m_tMaxAvg = pFirstTick->_t;
		m_bValid = true;
	}
}

} /* namespace orl */
