#include "SpanData.h"
#include "gtest/gtest.h"
namespace orl
{

class SpanDataTest
{
public:
	static const std::queue<SpanData::TICK>& getQueue (const SpanData& d) {return d.m_qTicks;}
	static double getAvg (const SpanData& d) {return d.m_fAvg;}
};

TEST(SpanDataTest, ConstructorTest)
{
	SpanData data;

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(0));
	EXPECT_DOUBLE_EQ(0.0, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(0.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(0, data.getStartTimeStamp());
}

//Left border - all ticks with span less then 1h - no max average, only current avg
//vvv
//....+....+....+....+....+....
TEST(SpanDataTest, StartWindowLess1h)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(1001, 2);
	data.addTick(1002, 3);
	data.moveSpanWindow(1002);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(3));
	EXPECT_DOUBLE_EQ(2.0, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(0.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(0, data.getStartTimeStamp());
}

//Left border - last ticks is exact 1h - all tick should be taken to maxAvg
//xxxxx
//....+....+....+....+....+....
TEST(SpanDataTest, StartWindowEq1h)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(1001, 2);
	data.addTick(1002, 3);
	data.addTick(1003, 4);
	data.addTick(4600, 5);
	data.moveSpanWindow(4600);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(5));
	EXPECT_DOUBLE_EQ(3.0, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(3.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

//Left border
//v   v    v    x    x
//....+....+....+....+....+....
TEST(SpanDataTest, StartWindowEvery1h)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(1000+3600, 2);
	data.addTick(1000+3600+3600, 3);
	data.addTick(1000+3600+3600+3600, 4);
	data.addTick(1000+3600+3600+3600+3600, 5);
	data.moveSpanWindow(1000+3600+3600+3600+3600);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(4.5, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(4.5, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000+3600+3600+3600, data.getStartTimeStamp());
}

//v   vvx       vv
//....+....+....+....+....+....
TEST(SpanDataTest, TestMiddleAvg)
{
	SpanData data;
	data.addTick(0, 0.1);
	data.addTick(1000, 1);
	data.addTick(1001, 2);
	data.addTick(1002, 3);
	data.addTick(1000+3600+3600, 5);
	data.addTick(1001+3600+3600, 6);
	data.moveSpanWindow(1001+3600+3600);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(5.5, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(3, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1002, data.getStartTimeStamp());
}

// x
// x
// x      v
//....+....+....+....+....+....
TEST(SpanDataTest, TestTheSameTimestampsBiggestLast)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(1000, 2);
	data.addTick(1000, 3);
	data.addTick(6000, 1);
	data.moveSpanWindow(6000);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

// x
// x
// x      v
//....+....+....+....+....+....
TEST(SpanDataTest, TestTheSameTimestampsBiggestFirst)
{
	SpanData data;
	data.addTick(1000, 3);
	data.addTick(1000, 2);
	data.addTick(1000, 1);
	data.addTick(6000, 1);
	data.moveSpanWindow(6000);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

// x
// x
// x      v
//....+....+....+....+....+....
TEST(SpanDataTest, TestTheSameTimestampsBiggestMiddle)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(1000, 3);
	data.addTick(1000, 2);
	data.addTick(6000, 1);
	data.moveSpanWindow(6000);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

// v x
// vvx      v
//....+....+....+....+....+....
TEST(SpanDataTest, TestMultiTheSameTimestamps1)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(1000, 2);
	data.addTick(1001, 3);
	data.addTick(1002, 4);
	data.addTick(1002, 5);
	data.addTick(8000, 1);
	data.moveSpanWindow(8000);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(4.5, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1002, data.getStartTimeStamp());
}

// x x
// xxx  v
//....+....+....+....+....+....
TEST(SpanDataTest, TestMultiTheSameTimestamps2)
{
	SpanData data;
	data.addTick(0, 0);
	data.addTick(1000, 1);
	data.addTick(1000, 3);
	data.addTick(1001, 4);
	data.addTick(1002, 4);
	data.addTick(1002, 5);
	data.addTick(4600, 1);
	data.moveSpanWindow(4600);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(4));
	EXPECT_DOUBLE_EQ(3.0, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(3.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}


//Right border - all ticks closer then 1h to the end are skipped
//x                          vv
//....+....+....+....+....+....
TEST(SpanDataTest, EndWindowLess1h)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(10001, 2);
	data.addTick(10002, 3);
	data.moveSpanWindow(10002);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(2.5, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(1.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

//Right border - to average all ticks from the last 1h are taken
//v                  x    x
//....+....+....+....+....+
TEST(SpanDataTest, EndWindowEq1h)
{
	SpanData data;
	data.addTick(1000, 1);
	data.addTick(10000, 2);
	data.addTick(13600, 3);
	data.moveSpanWindow(13600);

	EXPECT_EQ(SpanDataTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(2.5, SpanDataTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2.5, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(10000, data.getStartTimeStamp());
}

TEST(SpanDataTest, UnorderedTick)
{
	SpanData data;
	data.addTick(1000, 1);
	ASSERT_THROW(data.addTick(999, 2), std::runtime_error);
}

} /* namespace orl */
