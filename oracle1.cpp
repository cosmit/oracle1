#include <iostream>
#include "LogAvgCalculator.h"

int main(int argc, char *argv[])
{
	if (argc<2)
	{
		std::cerr << "No input file" << std::endl;
		return 1;
	}

	time_t startTime;
	double maxAverage;
	std::string userName;
	if ( !orl::LogAvgCalculator().maxAverage(argv[1], startTime, maxAverage, userName) )
	{
		std::cerr << "Something was wrong" << std::endl;
		return 1;
	}

	std::cout << "MaxAvg: " << maxAverage << " User: " << userName << " Start timestamp: " << startTime << std::endl;
	return 0;
}
