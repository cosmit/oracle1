#include "SpanDataUniqueTimestamp.h"
#include <stdexcept>

namespace orl
{

void SpanDataUniqueTimestamp::addTick(time_t t, double v)
{
	moveSpanWindow(t, false);

	//add new value and update average
	m_fAvg = (m_fAvg * m_qTicks.size() + v ) / (m_qTicks.size() + 1);
	m_qTicks.emplace(t, v);
}

void SpanDataUniqueTimestamp::moveSpanWindow(time_t t, bool bCheckEqual)
{
	if (t < m_tLast)
		throw std::runtime_error("Unordered tick");

	m_tLast = t;

	//remove items older then 1h and update average and maxAverage
	const TICK* pFirstTick = m_qTicks.empty() ? nullptr : &m_qTicks.front();
	while (pFirstTick && (t - pFirstTick->first > SPAN_WINDOW))
	{
		if (m_fAvg > m_fMaxAvg)
		{
			m_fMaxAvg = m_fAvg;
			m_tMaxAvg = pFirstTick->first;
		}

		if (m_qTicks.size() > 1)
			m_fAvg = (m_fAvg * m_qTicks.size() - pFirstTick->second) / (m_qTicks.size() - 1);
		else
			m_fAvg = 0;

		m_qTicks.pop();

		pFirstTick = m_qTicks.empty() ? nullptr : &m_qTicks.front();
	}

	//update maxAvg if span between first tick's time and new time is exact 1h
	//typically at the end of processing
	if (bCheckEqual && pFirstTick && (t - pFirstTick->first == SPAN_WINDOW) && (m_fAvg > m_fMaxAvg))
	{
		m_fMaxAvg = m_fAvg;
		m_tMaxAvg = pFirstTick->first;
	}
}

} /* namespace orl */
