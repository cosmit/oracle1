#include "SpanDataUniqueTimestamp.h"
#include "gtest/gtest.h"
namespace orl
{

class SpanDataUniqueTimestampTest
{
public:
	static const std::queue<SpanDataUniqueTimestamp::TICK>& getQueue (const SpanDataUniqueTimestamp& d) {return d.m_qTicks;}
	static double getAvg (const SpanDataUniqueTimestamp& d) {return d.m_fAvg;}
};

TEST(SpanDataUniqueTimestampTest, ConstructorTest)
{
	SpanDataUniqueTimestamp data;

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(0));
	EXPECT_DOUBLE_EQ(0.0, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(0.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(0, data.getStartTimeStamp());
}

//Left border - all ticks with span less then 1h - no max average, only current avg
//vvv
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, StartWindowLess1h)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(1001, 2);
	data.addTick(1002, 3);
	data.moveSpanWindow(1002);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(3));
	EXPECT_DOUBLE_EQ(2.0, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(0.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(0, data.getStartTimeStamp());
}

//Left border - last ticks is exact 1h - all tick should be taken to maxAvg
//xxxxx
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, StartWindowEq1h)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(1001, 2);
	data.addTick(1002, 3);
	data.addTick(1003, 4);
	data.addTick(4600, 5);
	data.moveSpanWindow(4600);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(5));
	EXPECT_DOUBLE_EQ(3.0, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(3.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

//Left border
//v   v    v    x    x
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, StartWindowEvery1h)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(1000+3600, 2);
	data.addTick(1000+3600+3600, 3);
	data.addTick(1000+3600+3600+3600, 4);
	data.addTick(1000+3600+3600+3600+3600, 5);
	data.moveSpanWindow(1000+3600+3600+3600+3600);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(4.5, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(4.5, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000+3600+3600+3600, data.getStartTimeStamp());
}

//v   vvx       vv
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, TestMiddleAvg)
{
	SpanDataUniqueTimestamp data;
	data.addTick(0, 0.1);
	data.addTick(1000, 1);
	data.addTick(1001, 2);
	data.addTick(1002, 3);
	data.addTick(1000+3600+3600, 5);
	data.addTick(1001+3600+3600, 6);
	data.moveSpanWindow(1001+3600+3600);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(5.5, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(3, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1002, data.getStartTimeStamp());
}

// v
// v
// x      v
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, TestTheSameTimestampsBiggestLast)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(1000, 2);
	data.addTick(1000, 3);
	data.addTick(6000, 1);
	data.moveSpanWindow(6000);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(3, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

// x
// v
// v      v
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, TestTheSameTimestampsBiggestFirst)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 3);
	data.addTick(1000, 2);
	data.addTick(1000, 1);
	data.addTick(6000, 1);
	data.moveSpanWindow(6000);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

// v
// x
// x      v
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, TestTheSameTimestampsBiggestMiddle)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(1000, 3);
	data.addTick(1000, 2);
	data.addTick(6000, 1);
	data.moveSpanWindow(6000);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(1));
	EXPECT_DOUBLE_EQ(1, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2.5, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}


//Right border - all ticks closer then 1h to the end are skipped
//x                          vv
//....+....+....+....+....+....
TEST(SpanDataUniqueTimestampTest, EndWindowLess1h)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(10001, 2);
	data.addTick(10002, 3);
	data.moveSpanWindow(10002);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(2.5, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(1.0, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(1000, data.getStartTimeStamp());
}

//Right border - to average all ticks from the last 1h are taken
//v                  x    x
//....+....+....+....+....+
TEST(SpanDataUniqueTimestampTest, EndWindowEq1h)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	data.addTick(10000, 2);
	data.addTick(13600, 3);
	data.moveSpanWindow(13600);

	EXPECT_EQ(SpanDataUniqueTimestampTest::getQueue(data).size(), static_cast<size_t>(2));
	EXPECT_DOUBLE_EQ(2.5, SpanDataUniqueTimestampTest::getAvg(data));
	EXPECT_DOUBLE_EQ(2.5, data.getMaxAvg());
	EXPECT_DOUBLE_EQ(10000, data.getStartTimeStamp());
}

TEST(SpanDataUniqueTimestampTest, UnorderedTick)
{
	SpanDataUniqueTimestamp data;
	data.addTick(1000, 1);
	ASSERT_THROW(data.addTick(999, 2), std::runtime_error);
}

} /* namespace orl */
