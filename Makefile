CXXFLAGS =	-O0 -g -Wall -fmessage-length=0 -std=gnu++0x -DGTEST_OS_CYGWIN=1 -Iinclude -Itests/gtest -I../tests/gtest -Igtest

OBJS =		oracle1.o SpanData.o LogAvgCalculator.o
TEST_OBJS = SpanData.o SpanData_test.o SpanDataUniqueTimestamp.o SpanDataUniqueTimestamp_test.o \
LogAvgCalculator.o LogAvgCalculator_test.o gtest_main.o gtest/src/gtest-all.o

LIBS = -lpthread 

TARGET =	oracle1
TEST_TARGET =	oracle1_test

gtest/src/%.o: gtest/src%.cpp
	$(CXX) -o "$@" "$<"
	
$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

$(TEST_TARGET):	$(TEST_OBJS)
	$(CXX) -o $(TEST_TARGET) $(TEST_OBJS) $(LIBS)
	
all:	$(TARGET)

test:	$(TEST_TARGET)

clean:
	rm -f $(OBJS) $(TARGET) $(TEST_OBJS) $(TEST_TARGET)
