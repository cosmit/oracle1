#include "LogAvgCalculator.h"

#include <fstream>
#include <iostream>

namespace orl
{

bool LogAvgCalculator::maxAverage(const char *filename, time_t& startTime, double& maxAverage, std::string& userName)
{
	m_mapUsers.clear();

	std::ifstream inFile(filename, std::ifstream::in);
	if (!inFile)
		return false;

	return parseStream(inFile, startTime, maxAverage, userName);
};

bool LogAvgCalculator::parseStream(std::istream& is, time_t& startTime, double& maxAverage, std::string& userName)
{
	std::string strLine, strUser;
	time_t timestamp, timestampLast=0;
	double value;

	while (std::getline(is, strLine))
	{
		if (!parseLine(strLine, timestamp, value, strUser))
		{
			//TRACE_MSG2("Not expected format line: ", strLine);
			continue;
		}

		try
		{
		  m_mapUsers[strUser].addTick(timestamp, value);
		}
		catch (std::runtime_error&)
		{
			//TRACE_MSG2("Unordered tick: ", strLine);
			continue;
		}

		timestampLast = std::max(timestamp, timestampLast);
	}

	if (m_mapUsers.empty())
		return false;

	//move time window to the end and search maxAverage data
	const MAP_USERDATA::mapped_type* pMaxData = nullptr;
	MAP_USERDATA::mapped_type* pCurrData;
	for (auto it : m_mapUsers)
	{
		pCurrData = &m_mapUsers[it.first];
		pCurrData->moveSpanWindow(timestampLast);
		if (!pCurrData->isValid())
			continue;

		if (!pMaxData || pCurrData->getMaxAvg() > pMaxData->getMaxAvg())
		{
			pMaxData = pCurrData;
			userName = it.first;
		}
	}

	if (pMaxData)
	{
		startTime = pMaxData->getStartTimeStamp();
		maxAverage = pMaxData->getMaxAvg();
		return true;
	}
	else
	{
		return false;	//there is no possibility to be there, but to avoid compilator's warning
	}
};

//',' is hardcoded as separator
//it means that ',' cannot be used as decimal-point character - depends on locale settings
//expected format TIMESTAMP,USER,VALUE
//TIMESTAMP - unsigned long long
//USER - text
//VALUE - float/double
bool LogAvgCalculator::parseLine(const std::string strLine, time_t& timestamp, double& value, std::string& user) const
{
	std::size_t nPos1=strLine.find(',');
	if (nPos1 == std::string::npos)
		return false;

	std::size_t nPos2=strLine.find(',', nPos1+1);
	if (nPos2 == std::string::npos)
		return false;

	if (nPos2<=nPos1)
		return false;

	try
	{
		std::size_t nPos;
		timestamp = std::stoull(strLine, &nPos);
		if (nPos != nPos1)
			return false;

		value = std::stod(strLine.substr(nPos2+1), &nPos);
		if (nPos != strLine.size() - nPos2 - 1) //check if we parse whole line
			return false;
	}
	catch(std::invalid_argument&)
	{
	 	 return false;
	}
	catch(std::out_of_range&)
	{
		return false;
	}

	user = strLine.substr(nPos1+1, nPos2-nPos1-1);

	if (user.empty())
		return false;

	return true;
}

} /* namespace orl */
