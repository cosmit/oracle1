#pragma once

#include <queue>
#include <utility>
#include <ctime>

namespace orl
{

class SpanDataUniqueTimestamp
{
	friend class SpanDataUniqueTimestampTest;

public:
	void addTick(time_t t, double v);
	void moveSpanWindow(time_t t, bool bCheckEqual=true);
	double getMaxAvg() const {return m_fMaxAvg;}
	time_t getStartTimeStamp() const {return m_tMaxAvg;}

private:
	typedef std::pair<time_t, double> TICK;
	static constexpr time_t SPAN_WINDOW = 3600; //1h

	std::queue<TICK> m_qTicks;
	double m_fAvg = 0;
	double m_fMaxAvg = 0;
	time_t m_tMaxAvg = 0;
	time_t m_tLast = 0;
};

} /* namespace orl */

