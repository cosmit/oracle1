#include "LogAvgCalculator.h"
#include "gtest/gtest.h"
#include <sstream>

namespace orl
{

class LogAvgCalculatorTest
{
public:
	LogAvgCalculatorTest(LogAvgCalculator& log) : m_log(log) {}

	const LogAvgCalculator::MAP_USERDATA& getMap (){return m_log.m_mapUsers;};

	bool parseLine(const char* strLine)
		{return m_log.parseLine(strLine, m_t, m_v, m_u);}

	bool parseStream(const char* str)
	{
		std::istringstream is(str);
		return m_log.parseStream(is, m_t, m_v, m_u);
	}

	double m_v;
	time_t m_t;
	std::string m_u;

private:
	LogAvgCalculator& m_log;
};

TEST(LogAvgCalculatorTest1, ConstructorTest)
{
	LogAvgCalculator log;
	EXPECT_EQ(LogAvgCalculatorTest(log).getMap().size(), static_cast<size_t>(0));
}

TEST(LogAvgCalculatorTest1, ParseLineCorrect)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_TRUE (wrapper.parseLine("1000,USER1,1.0"));

	EXPECT_EQ(wrapper.m_t, 1000);
	EXPECT_EQ(wrapper.m_u, "USER1");
	EXPECT_DOUBLE_EQ(wrapper.m_v, 1.0);
}

TEST(LogAvgCalculatorTest1, ParseLineWithSpace)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_TRUE (wrapper.parseLine("1000, USER1, 1.0"));
	EXPECT_EQ(wrapper.m_u, " USER1"); //no trim
}

TEST(LogAvgCalculatorTest1, ParseLineNoTimestamp)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine(",USER1,1.0"));
}

TEST(LogAvgCalculatorTest1, ParseLineWrongTimestamp)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("aa,USER1,1.0"));
}

TEST(LogAvgCalculatorTest1, ParseLineIncorrectTimestamp)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("123a,USER1,1.0"));
}

TEST(LogAvgCalculatorTest1, ParseLineToBigTimestamp)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("999999999999999999999999999,USER1,1.0"));
}

TEST(LogAvgCalculatorTest1, ParseLineNoUser)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("1000,,1.0"));
}

TEST(LogAvgCalculatorTest1, ParseLineNoValue)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("1000,USER1,"));
}

TEST(LogAvgCalculatorTest1, ParseLineWrongValue)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("1000,USER1,aaa"));
}

TEST(LogAvgCalculatorTest1, ParseLineIncorrectValue)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("1000,USER1,1.0aaa"));
}

TEST(LogAvgCalculatorTest1, ParseLineTooMuchColumns)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseLine("1000,USER1,1.0,2.0"));
}

TEST(LogAvgCalculatorTest1, ParseLineStream1)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_TRUE (wrapper.parseStream(
			"1000,USER1,1.0\n"
			"2000,USER1,2.0\n"
			"4600,USER1,3.0\n"
			));
	EXPECT_EQ(wrapper.m_t, 1000);
	EXPECT_EQ(wrapper.m_u, "USER1");
	EXPECT_DOUBLE_EQ(wrapper.m_v, 2.0);
}

TEST(LogAvgCalculatorTest1, ParseLineStream2)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_TRUE (wrapper.parseStream(
			"1000,USER1,1.0\n"
			"2000,USER2,2.0\n"
			"4600,USER1,3.0\n"
			));
	EXPECT_EQ(wrapper.m_t, 1000);
	EXPECT_EQ(wrapper.m_u, "USER1");
	EXPECT_DOUBLE_EQ(wrapper.m_v, 2.0);
}

TEST(LogAvgCalculatorTest1, ParseLineStream3)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_FALSE (wrapper.parseStream(
			"1000,USER1,1.0\n"
			"2000,USER1,2.0\n"
			"4599,USER1,3.0\n"
			));
}

TEST(LogAvgCalculatorTest1, ParseLineStream4)
{
	LogAvgCalculator log;
	LogAvgCalculatorTest wrapper(log);

	ASSERT_TRUE (wrapper.parseStream(
			"1000,USER1,1.0\n"
			"2000,USER2,7.0\n"
			"4600,USER1,3.0\n"
			"5700,USER1,1.0\n"
			));

	EXPECT_EQ(wrapper.m_t, 2000);
	EXPECT_EQ(wrapper.m_u, "USER2");
	EXPECT_DOUBLE_EQ(wrapper.m_v, 7.0);
}


} /* namespace orl */
